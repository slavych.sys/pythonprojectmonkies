from django.forms import ModelForm, ModelChoiceField
from django.forms import CharField, HiddenInput
from . import models


class RegisterForm(ModelForm):
    class Meta:
        model = models.CustomUser
        fields = ['username', 'is_private', 'first_name', 'last_name', 'password']


class LoginForm(ModelForm):
    class Meta:
        model = models.CustomUser
        fields = ['username', 'password']


class MonkeyForm(ModelForm):
    class Meta:
        model = models.Monkey
        fields = '__all__'


class BADForm(ModelForm):
    class Meta:
        model = models.Supplement
        fields = '__all__'


class BADNewAccessForm(ModelForm):
    scientist = CharField(max_length=50, label='Scientist username')

    class Meta:
        model = models.SupplementAccess
        fields = ['supplement']
        widgets = {
            "supplement": HiddenInput
        }

    def __init__(self, supplement, *args, **kwargs):
        super(BADNewAccessForm, self).__init__(*args, **kwargs)
        self.fields['supplement'].queryset = models.Supplement.objects.all().filter(name=supplement)


class SupplementModelChoiceField(ModelChoiceField):
    def label_from_instance(self, supplement):
        label = f"{supplement.name} ({supplement.duration} days)"
        return label


class ExperimentForm(ModelForm):
    class Meta:
        model = models.Experiment
        fields = '__all__'
        field_classes = {
            'supplement': SupplementModelChoiceField
        }
        widgets = {
            "monkey": HiddenInput()
        }

    def __init__(self, user, monkey, *args, **kwargs):
        super(ExperimentForm, self).__init__(*args, **kwargs)
        self.fields['supplement'].queryset = models.Supplement.objects.all().filter(supAccess__user=user)
        self.fields['monkey'].queryset = models.Monkey.objects.all().filter(name=monkey)


