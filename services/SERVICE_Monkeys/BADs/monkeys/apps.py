from django.apps import AppConfig


class MonkiesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'monkeys'
