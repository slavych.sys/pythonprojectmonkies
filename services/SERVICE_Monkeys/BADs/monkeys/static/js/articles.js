function ObjToHTML(jsonData) {
    if(jsonData['error']!=undefined)
        return '<div class="error message">' + jsonData.error + '</div>'
    if(jsonData['info']!=undefined)
        return '<div class="info message">' + jsonData.info + '</div>'
    return '<div class="error message">Unexpected data</div>'
}

function getReportDetails(report) {
    reportDetails = document.getElementById(report).querySelector('.reportDetails')
    if (reportDetails.innerHTML == '')
        fetch(url=window.location.href+"/"+report)
            .then(response => { if(response.headers.get("Content-Type") == 'text/html; charset=utf-8')
                                    return response.text()
                                if(response.headers.get("Content-Type") == 'application/json')
                                    return response.json().then(data => ObjToHTML(data))
                                return ObjToHTML({error: 'Unexpected response'})
                              }
                )
            .then(result => reportDetails.innerHTML=result)
    else{
        if(reportDetails.style.display == 'none')
            reportDetails.style.display = 'block';
        else
            reportDetails.style.display = 'none'
        }
}