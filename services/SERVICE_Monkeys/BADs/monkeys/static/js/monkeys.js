
function ObjToHTML(jsonData) {
    if(jsonData['error']!=undefined)
        return '<div class="error message">' + jsonData.error + '</div>'
    if(jsonData['info']!=undefined){
        if(jsonData.info['authors']!=undefined && jsonData.info['BAD']!=undefined) {
            var username = JSON.parse(document.getElementById('user_name').textContent)
            if(jsonData.info.authors.includes(username))
                return '<div class="info message">Monkey is busy with ' + jsonData.info.BAD + '</div>'
            }
            return '<div class="info message">' + jsonData.info.authors + ' occupied the monkey</div>'
        }
    return '<div class="error message">Unexpected data</div>'
}

function getMonkeyInfo(evt) {
    evt.preventDefault()
    if(evt.target.id == "searchform")
        monkey=evt.target.elements["searchinput"].value
    else
        monkey=evt.target.id
    fetch(url=window.location.href+"/"+monkey)
        .then(response => { if(response.headers.get("Content-Type") == 'text/html; charset=utf-8')
                                return response.text()
                            if(response.headers.get("Content-Type") == 'application/json')
                                return response.json().then(data => ObjToHTML(data))
                            return ObjToHTML({error: 'Unexpected response'})
                          }
            )
        .then(result => document.getElementById("varArea").innerHTML=result)

}

function get10freeMonkeys(evt) {
    evt.preventDefault()
    fetch(url=window.location.href+"/top10")
        .then(response => { if(response.headers.get("Content-Type") == 'text/html; charset=utf-8')
                                return response.text()
                            if(response.headers.get("Content-Type") == 'application/json')
                                return response.json().then(data => ObjToHTML(data))
                            return ObjToHTML({error: 'Unexpected response'})
                          }
            )
        .then(result => document.getElementById("varArea").innerHTML=result)

}