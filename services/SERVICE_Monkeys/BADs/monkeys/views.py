from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from . import forms, models
from django.db.models import Prefetch, F, Count, Q, IntegerField, DateTimeField
from django.db.models.functions import Cast
from datetime import timedelta
from django.utils.timezone import now
# Create your views here.


def register_page(request):
    if request.method == 'POST':
        user = forms.RegisterForm(request.POST)
        if user.is_valid():
            user = user.save()
            user.set_password(user.password)
            user.save()
            user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
            if user is not None:
                login(request, user)
                return redirect('reports')
            return redirect('login')
    return render(request, 'Register.html', {'form': forms.RegisterForm,
                                             'type': False})


def login_page(request):
    if request.method == 'POST':
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            return redirect('reports')
        return redirect('register')
    return render(request, 'Register.html', {'form': forms.LoginForm,
                                             'type': True})


def logout_page(request):
    logout(request)
    return redirect('login')


def reports_list(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            try:
                supplement = models.Supplement.objects.get(name=request.POST['BAD'])
            except models.Supplement.DoesNotExist:
                return JsonResponse({'error': 'No such BAD'})
            try:
                models.SupplementAccess.objects.get(supplement=supplement, user=request.user)
            except models.SupplementAccess.DoesNotExist:
                return JsonResponse({'error': 'No access'})
            report = models.Report(supplement=supplement)
            report.save()
        return redirect('reports')
    data = models.Report.objects.all().order_by('-createddatetime')[:50]\
                .prefetch_related(Prefetch("supplement__supAccess",
                                           queryset=models.SupplementAccess.objects.all().filter(user__is_private=0),
                                           to_attr='authors'))[:100]
    return render(request, 'ReportsListPage.html', {'data': data})


def reports_details(request, report):
    if request.user.is_authenticated:
        try:
            report = models.Report.objects.get(id=report)
        except models.Report.DoesNotExist:
            return JsonResponse({'error': 'No such report'})
        data = models.Experiment.objects.all()\
            .filter(supplement=report.supplement)\
            .annotate(lifespanperreport=Cast((report.createddatetime - F('createddatetime'))/timedelta(seconds=60), output_field=IntegerField()))\
            .filter(supplement__duration__lt=F('lifespanperreport'))
        if len(data) == 0:
            return JsonResponse({'info': 'No experiment in this report'})
        return render(request, 'ReportDetails.html', {'data': data})
    return JsonResponse({'info': 'Log in or Register'})


def monkeys_mainpage(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            monkey = forms.MonkeyForm(request.POST)
            if monkey.is_valid():
                monkey.save()
        return render(request, 'Monkeys.html', {
                                                'data': get_free_monkeys_top10(),
                                                'newMonkeyForm': forms.MonkeyForm})
    return redirect('login')


def bads_list(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            supplement = forms.BADForm(request.POST)
            if supplement.is_valid():
                supplement = supplement.save()
                access = models.SupplementAccess()
                access.user = request.user
                access.supplement = supplement
                access.save()
        data = models.Supplement.objects.all().annotate(access=Count("supAccess", Q(supAccess__user=request.user))).filter(access=1)
        return render(request, 'MyBADs.html', {
                                                'data': data,
                                                'newBADForm': forms.BADForm})
    return redirect('login')


def bad_info(request, bad):
    if request.user.is_authenticated:
        if request.method == 'POST':
            access = models.SupplementAccess()
            try:
                access.user = models.CustomUser.objects.get(username=request.POST['scientist'])
            except models.Report.DoesNotExist:
                return JsonResponse({'error': 'No such user'})
            try:
                access.supplement = models.Supplement.objects.get(id=request.POST['supplement'])
            except models.Report.DoesNotExist:
                return JsonResponse({'error': 'No such BAD'})
            access.save()
        accesses = models.SupplementAccess.objects.filter(supplement__name=bad)
        data = models.Experiment.objects.filter(supplement__name=bad)
        return render(request, 'BADInfo.html', {
                                                'BAD': bad,
                                                'data': data,
                                                'accesses': accesses,
                                                'newAccessForm': forms.BADNewAccessForm(supplement=bad,
                                                                                        initial={'supplement': models.Supplement.objects.get(name=bad)})
        })
    return redirect('login')


def monkey_info(request, monkey):
    if request.user.is_authenticated:
        if request.method == 'POST':
            experiment = forms.ExperimentForm(data=request.POST, monkey=monkey, user=request.user)
            if experiment.is_valid():
                experiment.save()
            return redirect('monkeys')
        try:
            monkeyline = models.Monkey.objects.get(name=monkey)
        except models.Monkey.DoesNotExist:
            return JsonResponse({"error": "No such monkey"})
        currentexperiment = models.Experiment.objects.all()\
            .filter(monkey=monkeyline)\
            .filter(supplement__duration__gt=Cast((now() - F('createddatetime'))/60000000, output_field=IntegerField()))
        if len(currentexperiment) > 0:
            return JsonResponse({"info": {"authors": list(currentexperiment[0].supplement.supAccess.all().values_list('user__username', flat=True)),
                                          "BAD": currentexperiment[0].supplement.name},
                                 })
        data = models.Experiment.objects.all()\
            .filter(monkey=monkeyline)\
            .filter(supplement__duration__lt=Cast((F('supplement__supReport__createddatetime') - F('createddatetime'))/60000000, output_field=IntegerField()))
        return render(request, 'MonkeyInfo.html', {
                                                'monkey': monkey,
                                                'data': data,
                                                'newExperimentForm': forms.ExperimentForm(monkey=monkey,
                                                                                          user=request.user,
                                                                                          initial={'monkey': models.Monkey.objects.get(name=monkey),
                                                                                                    'user': request.user})
        })
    return redirect('login')


def monkeys_list(request):
    if request.user.is_authenticated:
        return render(request, 'FreeMonkeysList.html', {'data': get_free_monkeys_top10()})
    redirect('login')


def get_free_monkeys_top10():
    return models.Monkey.objects.all()\
            .annotate(duringExp=Count("monkeyExperiment", Q(monkeyExperiment__supplement__duration__gt=Cast((now() - F('monkeyExperiment__createddatetime'))/60000000, output_field=IntegerField()))))\
            .filter(duringExp=0)[:10]
    