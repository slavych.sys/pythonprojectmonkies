from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(unique=True, max_length=50, verbose_name='Username')
    is_private = models.BooleanField(default=False, verbose_name='Assistant')
    first_name = models.CharField(max_length=150, verbose_name='First name')
    last_name = models.CharField(max_length=150, verbose_name='Last name')
    is_superuser = None

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.username


class Monkey(models.Model):
    name = models.CharField(unique=True, max_length=50, verbose_name='Monkey unique name')



class Supplement(models.Model):
    name = models.CharField(unique=True, max_length=50, verbose_name='Name')
    composition = models.TextField(verbose_name='Composition')
    duration = models.IntegerField(verbose_name='Duration (in days)')


class SupplementAccess(models.Model):
    supplement = models.ForeignKey(Supplement, related_name='supAccess', on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, related_name='userAccess', on_delete=models.CASCADE)

    class Meta:
        unique_together = ['supplement', 'user']


class Experiment(models.Model):
    supplement = models.ForeignKey(Supplement, related_name='supExperiment', on_delete=models.CASCADE)
    monkey = models.ForeignKey(Monkey, related_name='monkeyExperiment', on_delete=models.CASCADE)
    createddatetime = models.DateTimeField(auto_now_add=True)
    effect = models.TextField(verbose_name='Effect')


class Report(models.Model):
    supplement = models.ForeignKey(Supplement, related_name='supReport', on_delete=models.CASCADE)
    createddatetime = models.DateTimeField(auto_now_add=True)

