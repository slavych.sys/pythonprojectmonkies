from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import gettext_lazy as _


class CustomUserManager(BaseUserManager):
    def create_user(self, username, password, **extra_fields):
        if not username or password or extra_fields.get('first_name') is None or extra_fields.get('last_name') is None:
            raise ValueError(_("All fields must be set"))
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save()
        return user
