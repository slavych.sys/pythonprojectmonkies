"""
URL configuration for BADs project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.shortcuts import redirect
from monkeys import views

urlpatterns = [
    path('login', views.login_page, name='login'),
    path('logout', views.logout_page, name='logout'),
    path('register', views.register_page, name='register'),
    path('', lambda request: redirect('reports', permanent=False)),
    path('reports', views.reports_list, name='reports'),
    path('reports/<int:report>', views.reports_details, name='reportDetails'),
    path('monkeys', views.monkeys_mainpage, name='monkeys'),
    path('monkeys/top10', views.monkeys_list, name='top10monkeys'),
    path('myBADs', views.bads_list, name='myBADs'),
    path('BAD/<str:bad>', views.bad_info, name='BAD'),
    path('monkeys/<str:monkey>', views.monkey_info, name='monkey'),

]

