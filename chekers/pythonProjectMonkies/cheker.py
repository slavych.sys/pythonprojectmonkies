#!/usr/bin/env python3
import jwt
import requests
from bs4 import BeautifulSoup
import sys
import uuid
from enum import Enum
import random
import time
import itertools
import os
import json

LOGFILE = sys.stderr

class ExitStatus(Enum):
    OK = 101
    CORRUPT = 102
    MUMBLE = 103
    DOWN = 104
    CHECKER_ERROR = 110


class ServieceFunc():
    headers = {'User-Agent':	'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:101.0) Gecko/20100101 Firefox/101.0'}
    timeout = 10
    def __init__(self, _host, _username=None, _password=None, _isAssistant=0):
        self.host = _host
        self.session = requests.session()
        self.session.headers.update(self.headers)
        if _username != None:
            self.username = _username
        else:
            self.username = str(uuid.uuid4())
        if _password != None:
            self.password = _password
            url = _host + '/login'
            data = {}
        else:
            self.password = str(uuid.uuid4())
            url = _host + '/register'
            data = {
                'first_name': str(uuid.uuid4()),
                'last_name': str(uuid.uuid4()),
                'is_private': 'on' if _isAssistant else 'off'
                }
        csrfToken = self.session.get(url=url, timeout=self.timeout)
        if csrfToken.status_code != 200:
            print('login faild - service down!', csrfToken.status_code, sep=' -- ', file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(csrfToken.text, 'lxml').find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('login faild - csrf not find!', file=LOGFILE)
            exit(ExitStatus.MUMBLE.value)
        data['username'] = self.username
        data['password'] = self.password
        data['csrfmiddlewaretoken'] = csrfToken
        resp = self.session.post(url=url, data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('login faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        if resp.url.split('/')[-1] != 'reports':
            print('login faild - redirect ', resp.url, sep='' , file=LOGFILE)
            exit(ExitStatus.MUMBLE.value)

    def get_username(self):
        resp = self.session.get(url=self.host)
        if resp.status_code != 200:
            print('username check faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        fullname = BeautifulSoup(resp.text, 'lxml').find('div', {'class':'topLineHeader'}).text
        return fullname[fullname.find('(')+1:fullname.find(')')]
    
    def get_myBADs(self):
        url = self.host + '/myBADs'
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('my BADs listing faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        badList = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'myBADLine'})
        if len(badList) == 0:
            return badList
        ret = {}
        for bad in badList:
            ret[bad.find('a').text] = ((bad.find('p').text), bad.findAll('p')[1].text.split(': ')[1])
        return ret
    
    def create_BAD(self, _name, _description, _duration):
        url = self.host + '/myBADs'
        csrfToken = self.session.get(url=url, timeout=self.timeout)
        if csrfToken.status_code != 200:
            print('BAD creating faild - service down!', csrfToken.status_code, sep=' -- ', file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(csrfToken.text, 'lxml').find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('BAD creating faild - csrf not find!', file=LOGFILE)
            exit(ExitStatus.MUMBLE.value)
        data = {
            'csrfmiddlewaretoken': csrfToken,
            'name': _name,
            'composition': _description,
            'duration': _duration
            }
        resp = self.session.post(url=url, data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('BAD creating faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)

    def get_BADinfo(self, _name):
        url = self.host + '/BAD/' + _name
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('BAD info faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        accessList = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'accessLine'})
        if len(accessList) == 0:
            print('acess list is empty', file=LOGFILE)
            exit(ExitStatus.MUMBLE.value)
        retAcess = []
        for username in accessList:
            retAcess += [(username.find('td').text)]     
        experiments = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'experimentLine'})
        retExperiment = []   
        for experiment in experiments:
            retExperiment += [{
                'monkey': (experiment.findAll('td')[0].text),
                'effect': (experiment.findAll('td')[1].text),
                'start date': (experiment.findAll('td')[2].text)
                }]
        return retAcess, retExperiment
    
    def create_Access(self, _BADname, _username):
        url = self.host + '/BAD/' + _BADname
        BADinfo = self.session.get(url=url, timeout=self.timeout)
        if BADinfo.status_code != 200:
            print('BAD access faild - service down!', csrfToken.status_code, sep=' -- ', file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(BADinfo.text, 'lxml').find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('BAD access faild - csrf not find!', file=LOGFILE)
            exit(ExitStatus.MUMBLE.value)        
        try:
            BADid = BeautifulSoup(BADinfo.text, 'lxml').find('input', {'name':'supplement'}).get('value')
        except:
            print(Exception)
            print('BAD access faild - supplement id not find!', file=LOGFILE)
            exit(ExitStatus.MUMBLE.value)
        data = {
            'csrfmiddlewaretoken': csrfToken,
            'scientist': _username,
            'supplement': BADid
            }
        resp = self.session.post(url=url, data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('BAD access faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)

    def check_Access(self, _BADname, _username):
        accesses, _ = self.get_BADinfo(_BADname)
        if _username not in accesses:
            print('BAD access check faild - not listed', file=LOGFILE)
            return False
        return True

    def get_reports(self) -> dict:
        url = self.host + '/reports'
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('report listing faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        reportList = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'reportLine'})
        if len(reportList) == 0:
            print('report listing is empty', file=LOGFILE)
            exit(ExitStatus.MUMBLE.value)
        ret = {}
        for report in reportList:
            ret[(report.find('h2').text)] = {
                'id': report.find('td').get('id'),
                'authors': (report.find('p').text)
                }
        return ret

    def get_reportDetails(self, _reportheader):
        url = self.host + '/reports/' + self.get_reports()[_reportheader]['id']
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('report details faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        if resp.headers['Content-Type'] == 'application/json':
            return 0, resp.json()
        detailsList = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'detailLine'})
        ret = []   
        for detailLine in detailsList:
            header = detailLine.find('h3').text
            ret += [{
                'monkey': header.split(' ')[0],
                'datetime': header[len((header.split(' ')[0])):],
                'effect': detailLine.findAll('td')[1].text
                }]
        return ret

    def create_report(self, _BADname):
        url = self.host + '/BAD/' + _BADname
        BADinfo = self.session.get(url=url, timeout=self.timeout)
        if BADinfo.status_code != 200:
            print('BAD report faild - service down!', csrfToken.status_code, sep=' -- ', file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(BADinfo.text, 'lxml').find('form', {'id': 'reportForm'}).find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('BAD report faild - csrf not find!', file=LOGFILE)
            exit(ExitStatus.MUMBLE.value)     
        data = {
            'csrfmiddlewaretoken': csrfToken,
            'BAD': _BADname
            }
        resp = self.session.post(url=self.host + '/reports', data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('BAD report faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)

    def get_freeMonkeys(self):
        url = self.host + '/monkeys'
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('free monkeys faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        monkList = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'monkeyLine'})
        ret = []   
        for monk in monkList:
            ret += [monk.find('td').text]
        return ret
    
    def get_monkeyInfo(self, _name):
        url = self.host + '/monkeys/' + _name
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('monkey info faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value) 
        if resp.headers['Content-Type'] == 'application/json':
            return 0, resp.json()
        ret = {}  
        experiments = BeautifulSoup(resp.text, 'lxml').findAll('tr', {'class':'experimentLine'})
        for experiment in experiments:
            ret[experiment.find('h2')[0].text] = experiment.find('p')
        return 1, ret

    def create_monkey(self, _name):
        url = self.host + '/monkeys'
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('Creating monkey faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(resp.text, 'lxml').find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('Creating monkey faild - csrf not find!', file=LOGFILE)
            exit(ExitStatus.MUMBLE.value)       
        data = {
            'csrfmiddlewaretoken': csrfToken,
            'name': _name
            }
        resp = self.session.post(url=url, data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('Creating monkey faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)

    def create_experiment(self, _monkey, _BAD, _effect):
        url = self.host + '/monkeys/' + _monkey
        resp = self.session.get(url=url)
        if resp.status_code != 200:
            print('Creating experiment faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        try:
            csrfToken = BeautifulSoup(resp.text, 'lxml').find('input', {'name':'csrfmiddlewaretoken'}).get('value')
        except:
            print('Creating experiment faild - csrf not find!', file=LOGFILE)
            exit(ExitStatus.MUMBLE.value)
        BADids = BeautifulSoup(resp.text, 'lxml').findAll('option')
        BADid = -1
        for id in BADids:
            if id.text.split(' (')[0] == _BAD:
                BADid = id.get('value')
        if BADid == -1:
            print('Creating experiment faild - invalid BAD', file=LOGFILE)
            exit(ExitStatus.CHECKER_ERROR.value)
        data = {
            'csrfmiddlewaretoken': csrfToken,
            'supplement': BADid,
            'effect': _effect,
            'monkey': BeautifulSoup(resp.text, 'lxml').find('input', {'name':'monkey'}).get('value')
            }
        resp = self.session.post(url=url, data = data, timeout=self.timeout)
        if resp.status_code != 200:
            print('Creating monkey faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
        oldusers = get_oldusersList()
        if self.username not in oldusers:
            oldusers[self.username] = self.password
            write_oldusersList(oldusers)

    def clear(self):
        self.headers['Connection'] = 'close'
        self.session.headers.update(self.headers)
        resp = self.session.get(url=self.host + '/logout')
        if resp.status_code != 200:
            print('logout faild - response status: ', resp.status_code, sep='' , file=LOGFILE)
            exit(ExitStatus.DOWN.value)
    
def get_oldusersList():
    if os.path.isfile('oldusers.txt'):
        return(json.loads(open('oldusers.txt').read()))
    return {}

def write_oldusersList(oldusers):
    l = 0
    r = len(oldusers) 
    if r > 20:
        l = r - 20
    newU = dict(itertools.islice(oldusers.items(), l, r))
    print(json.dumps(newU), file=open('oldusers.txt', 'w'))

def info():
    print('1:1:2')
    exit(ExitStatus.OK.value)

def put(host, flag, vuln):
    if vuln == 0:
        putter = ServieceFunc(host)
        putter.get_myBADs()
        BADname = str(uuid.uuid4())
        putter.create_BAD(BADname, flag, random.randint(1, 40))
        monkey = putter.get_reportDetails(list(putter.get_reports().keys())[0])[0]['monkey']
        putter.get_freeMonkeys()
        state, info = putter.get_monkeyInfo(monkey)
        if state == 0:
            putter.create_report(BADname)
        else: 
            putter.create_experiment(monkey, BADname, str(uuid.uuid4()))
        print(putter.username, putter.password, BADname, sep=' | ')
        exit(ExitStatus.OK.value)
    if vuln == 1:
        olduser, oldpswd = get_oldusersList().popitem()
        putter = ServieceFunc(host, olduser, oldpswd)        
        badList = putter.get_myBADs()
        for i, k in badList.items():
            BADname = i
            _, chE = putter.get_BADinfo(BADname)
            if len(chE) > 0:
                break
        monkey = putter.get_freeMonkeys()
        if len(monkey) == 0:
            putter.create_monkey(str(uuid.uuid4()))
            putter.get_freeMonkeys()
            if len(monkey) == 0:
                print('empty monkeys list', file=sys.stderr)
                exit(ExitStatus.MUMBLE.value)
        monkey = monkey[0]
        putter.create_experiment(monkey, BADname, flag)
        putter.create_report(BADname)
        print(putter.username, putter.password, monkey, BADname, sep=' | ')
        exit(ExitStatus.OK.value)
    if vuln == 2:
        putter0 = ServieceFunc(host, _username=flag, _isAssistant=1)        
        olduser, oldpswd = get_oldusersList().popitem()
        putter = ServieceFunc(host, olduser, oldpswd)        
        badList = putter.get_myBADs()
        for i, k in badList.items():
            BADname = i
            _, chE = putter.get_BADinfo(BADname)
            if len(chE) > 0:
                break
        monkey = putter.get_freeMonkeys()
        if len(monkey) == 0:
            putter.create_monkey(str(uuid.uuid4()))
            putter.get_freeMonkeys()
            if len(monkey) == 0:
                print('empty monkeys list', file=sys.stderr)
                exit(ExitStatus.MUMBLE.value)
        monkey = monkey[0]
        putter.create_Access(BADname, flag)
        monkey = putter0.get_freeMonkeys()
        if len(monkey) == 0:
            putter0.create_monkey(str(uuid.uuid4()))
            putter0.get_freeMonkeys()
            if len(monkey) == 0:
                print('empty monkeys list', file=sys.stderr)
                exit(ExitStatus.MUMBLE.value)
        monkey = monkey[0]
        putter0.create_experiment(monkey, BADname, str(uuid.uuid4()))
        putter0.create_report(BADname)
        print(putter0.username, putter0.password, sep=' | ')
        exit(ExitStatus.OK.value)
    exit(ExitStatus.CHECKER_ERROR.value)

def get(host, fid, flag, vuln):
    flagId = fid.split(' | ')
    getter = ServieceFunc(host, flagId[0], flagId[1])
    if vuln == 0:
        myBADs = getter.get_myBADs()
        if flagId[2] not in myBADs:
            print('BAD not found: ', fid, file=sys.stderr)
            exit(ExitStatus.CORRUPT.value)
        comp, _ = myBADs[flagId[2]]
        if comp != flag:
            print('flag doesn`t match: ', fid, file=sys.stderr)
            exit(ExitStatus.CORRUPT.value)
        _, exprs = getter.get_BADinfo(flagId[2])
        if len(exprs) > 0:
            getter.create_report(flagId[2])
        exit(ExitStatus.OK.value)
    if vuln == 1:
        myBADs = getter.get_myBADs()
        if flagId[3] not in myBADs:
            print('BAD not found: ', fid, file=sys.stderr)
            exit(ExitStatus.CORRUPT.value)
        _, exprs = getter.get_BADinfo(flagId[3])
        for expr in exprs:
            if expr['monkey'] == flagId[2] and expr['effect'] == flag:
                exit(ExitStatus.OK.value)
        print('flag not found: ', flagId, file=sys.stderr)
        exit(ExitStatus.CORRUPT.value)
    if vuln == 2:
        if getter.get_username() == flag:
            exit(ExitStatus.OK.value)
        print('flag not found: ', fid, file=sys.stderr)
        exit(ExitStatus.CORRUPT.value)
    exit(ExitStatus.CHECKER_ERROR.value)

def check(host):
    hRand = random.randint(0, 2)
    hRand = 2
    print('line', hRand, sep=': ', file=sys.stderr)
    if hRand == 0:
        olduser, oldpswd = get_oldusersList().popitem()
        user1 = ServieceFunc(host, olduser, oldpswd)
        user2 = ServieceFunc(host, _isAssistant=1)
        badList = user1.get_myBADs()
        if len(badList) == 0:
            print('BAD list is empty', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value) 
        for i, k in badList.items():
            hBADname = i
            _, chE = user1.get_BADinfo(hBADname)
            if len(chE) > 0:
                break
        user1.create_Access(hBADname, user2.username)
        if not user1.check_Access(hBADname, user2.get_username()):
            print('Access chek faild', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value)
        hCnt = random.randint(2, 6)
        for i in range(0, hCnt):
            user2.create_monkey(str(uuid.uuid4()))  
        hmnk = user2.get_freeMonkeys()
        if len(hmnk) == 0:
            print('free monkeys list is empty', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value) 
        hmnk = hmnk[0]
        user2.create_experiment(hmnk, hBADname, str(uuid.uuid4()))
        stM, monkeyInfo = user1.get_monkeyInfo(hmnk)
        if stM != 0 or len(monkeyInfo) < 1: 
            print('unexpected monkey info', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value)  
        user1.create_report(hBADname)
        reportHeader, descr = user1.get_reports().popitem()
        if descr['id'] == None:
            print('not find report id in list page', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value)
        user1.get_reportDetails(reportHeader)
        hBADname = str(uuid.uuid4())
        user1.create_BAD(hBADname, str(uuid.uuid4()), random.randint(0, 45))
        user1.create_Access(hBADname, user2.get_username())
    elif hRand == 1:
        user1 = ServieceFunc(host, _isAssistant=1)
        hBADname = str(uuid.uuid4())
        user1.create_BAD(hBADname, str(uuid.uuid4()), random.randint(0, 45))
        badList = user1.get_myBADs()
        if len(badList) == 0:
            print('BAD list is empty', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value)
        olduser, oldpswd = get_oldusersList().popitem()
        user2 = ServieceFunc(host, olduser, oldpswd)
        user1.create_Access(hBADname, user2.username)
        if not user1.check_Access(hBADname, user2.get_username()):
            print('Access chek faild', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value)
        user2.create_monkey(str(uuid.uuid4()))  
        user2.create_monkey(str(uuid.uuid4()))  
        hmnk = user2.get_freeMonkeys()
        if len(hmnk) == 0:
            print('free monkeys list is empty', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value) 
        hmnk = hmnk[-1]
        user1.create_experiment(hmnk, hBADname, str(uuid.uuid4()))
        stM, monkeyInfo = user1.get_monkeyInfo(hmnk)
        if stM != 0 or len(monkeyInfo) < 1: 
            print('unexpected monkey info', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value)  
        user2.create_report(hBADname)
        reportHeader, descr = user1.get_reports().popitem()
        if descr['id'] == None:
            print('not find report id in list page', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value)
        user1.get_reportDetails(reportHeader)
    elif hRand == 2:
        user1 = ServieceFunc(host)
        BADname = str(uuid.uuid4())
        user1.create_BAD(BADname, str(uuid.uuid4()), random.randint(0, 45))
        badList = user1.get_myBADs()
        if len(badList) == 0:
            print('BAD list is empty', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value)
        olduser, oldpswd = get_oldusersList().popitem()
        user2 = ServieceFunc(host, olduser, oldpswd)
        badList = user2.get_myBADs()
        if len(badList) == 0:
            print('BAD list is empty', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value) 
        for i, k in badList.items():
            hBADname = i
            _, chE = user2.get_BADinfo(hBADname)
            if len(chE) > 0:
                break
        user2.create_Access(hBADname, user1.username)
        if not user2.check_Access(hBADname, user1.get_username()):
            print('Access chek faild', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value) 
        user1.create_monkey(str(uuid.uuid4()))  
        hmnk = user1.get_freeMonkeys()
        if len(hmnk) == 0:
            print('free monkeys list is empty', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value) 
        hmnk = hmnk[0]
        user1.create_experiment(hmnk, BADname, str(uuid.uuid4()))
        stM, monkeyInfo = user1.get_monkeyInfo(hmnk)
        if stM != 0 or len(monkeyInfo) < 1: 
            print('unexpected monkey info', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value)  
        user2.create_report(hBADname)
        reportHeader, descr = user1.get_reports().popitem()
        if descr['id'] == None:
            print('not find report id in list page', file=sys.stderr)
            exit(ExitStatus.MUMBLE.value)
        user1.get_reportDetails(reportHeader) 
    user1.clear()
    user2.clear()           
    exit(ExitStatus.OK.value)



def main():
    try:
        mode = sys.argv[1]
        host = sys.argv[2]
        if mode == "get":
            get(host, sys.argv[3], sys.argv[4], int(sys.argv[5]))
            print(ExitStatus.OK.value)
        elif mode == "put":
            put(host, sys.argv[4], int(sys.argv[5]))
        elif mode == "check":
            check(host)
        elif mode == "info":
            info()
        else:
            raise IndexError
    except IndexError:
        exit(ExitStatus.CHECKER_ERROR.value)


if __name__ == "__main__":
    main()